#lexer module for text decks

"""
	algorithms include:
		"statmux" - straight linear read (loops and can run in reverse)
		"burroughs" - burroughs columnar cut-up
		"markov" - markov chaining
		
	fx include:
		"antonym" - tries to sub in antonyms
		"synonym" - tries to sub in synonyms
		"repeat" - uses repetition
		"alliterate" - tries to use alliteration
		"caps" - ALL CAPS!
		"caml" - cAmElCaSe
"""

import markov
import textwrap

class Lexer:
    def __init__(self, dev):
        self.dev = dev
        self.mount = ""
        self.words = []
        self.rows = []
        self.markov = None
        self.burroughs = None
        self.algorithm = "statmux"
        self.position = 0
        self.velocity = 1
        self.name = "empty"

    def load(self, intext=None):
        if intext:
            text = open(intext).read()
            self.words = text.split()
            self.markov = markov.Markov(text)
            self.burroughs = textwrap.wrap(text, 44)
            self.name = intext.split("/")[-1]
        else:
            pass
            #mount the drive
            #glob text files and read the 1st one into buffer
            #unmount		
        
    #spits out next chunk    
    def get_word(self, inword):
        out = ""
        if self.algorithm == "statmux":
            out += self.words[self.position] + " "
        elif self.algorithm == "markov":
            out += self.markov.get_next(inword) + " "
        elif self.algorithm == "burroughs":
            out += self.burroughs.get_next()
        self.position += self.velocity 
        if self.position == len(self.words):
            self.position = 0
        return out
        
    def get_burr(self, offset):
        out = ""
        line = self.burroughs[self.position]
        words = line[offset:-1].split()
        #print words
        try:
	        if offset == 0:
	            return words[0] + " "
	        if line[offset-1].isspace():
	            return words[0] + " "
	        else:
	            return words[1] + " "
        except:
            self.position += self.velocity
            if self.position == len(self.burroughs):
                self.position = 0
            return self.get_burr(0)

        
        	
    
    #fetches next chunk chunk->fx or fx->chunk depending on fx type
    def fetch(self):
        pass
    
    def update_input(self, line):
        pass
        
