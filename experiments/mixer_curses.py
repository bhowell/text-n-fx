#!/usr/bin/env python
#
#  mixer_curses.py
#
#  copyright 2010 Brendan Howell
#  http://wintermute.org/brendan
#
# GNU General Public Licence (GPL)
# 
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#

import curses
import curses.ascii
import pygame
from pygame.locals import *

TEXTSTREAMEVENT = USEREVENT + 1
KBPOLLEVENT = USEREVENT + 2


class MixerScreen:
    def __init__(self):

        # initialize pygame stuff
        pygame.init()
                
        #main curses window
        self.screen = curses.initscr()
        # set curses to not delay waiting for input
        self.screen.nodelay(True)
        
        # turn off key echo
        curses.noecho()
        # interrupt on all keypress events
        curses.cbreak()
        # turn on color
        curses.start_color()

        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_BLUE)
        curses.init_pair(3, curses.COLOR_RED, curses.COLOR_BLUE)


        # setup background window, titles etc.        
        self.screen.addstr(0, 0, "DISK A",curses.color_pair(1))
        self.screen.addstr(0, 74, "DISK B", curses.color_pair(1))
        self.screen.addstr(0, 36, "WORD-UP", curses.color_pair(2))
        self.screen.hline(11, 20, curses.ACS_HLINE, 40)
        self.screen.addch(11, 40, curses.ACS_DARROW, curses.color_pair(3))
        self.screen.refresh()
        
        #left deck
        begin_x = 0 ; begin_y = 1
        height = 10 ; width = 40
        self.leftwin = curses.newwin(height, width, begin_y, begin_x)
        self.leftwin.border()
        self.leftwin.refresh()
        
        #right deck
        begin_x = 40
        self.rightwin = curses.newwin(height, width, begin_y, begin_x)
        self.rightwin.border()
        self.rightwin.refresh()
        
        #main mix out
        begin_x = 20; begin_y = 12
        self.outwin = curses.newwin(height, width, begin_y, begin_x)
        self.outwin.border()
        self.outwin.refresh()
        
        self.toggle = True

    def handleJoyEvent(self, evnt):
        if (self.toggle):
            self.outwin.addch(5, 5, "#", curses.color_pair(1))
        else:
            self.outwin.addch(5, 5, "*", curses.color_pair(2))
        self.toggle = not(self.toggle)

    def handleKey(self, key):
        if(key == curses.ascii.ESC):
            pygame.event.post(pygame.event.Event(QUIT))

    def updateTextStreams(self, event):
        pass

    def main(self):

        # set default timer for text stream updates and KB poll
        pygame.time.set_timer(TEXTSTREAMEVENT, 500)
        pygame.time.set_timer(KBPOLLEVENT, 10)

        while True:
            evnt = pygame.event.wait()
            if (evnt.type == JOYAXISMOTION):
                self.handleJoyEvent(evnt)
            elif (evnt.type == TEXTSTREAMEVENT):
                self.updateTextStreams(evnt)
            elif (evnt.type == QUIT):
                break
            else:
                c = self.screen.getch()
                if(c != -1):
                    self.handleKey(c)
                    

        # shut down / clean up
        pygame.quit()
        curses.nocbreak()
        curses.echo()
        curses.endwin()

if __name__ == "__main__":
    scr = MixerScreen()
    scr.main()
